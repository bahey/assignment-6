// By: A.R. Abdullah Al Bahey
// Course: Programming and Problem Solving
// Assignment on: Recursion
// Index No: 19020082


# include <stdio.h>

int fibonacciSeq(int n){

    if (n==0) return 0;
    else if (n == 1) return 1;
    else return fibonacciSeq(n-1)+fibonacciSeq(n-2);
}


int main() {
    int num, n;
    
    printf("How many lines you want to print ? \n");
    scanf("%d", &n);
    	printf("\n");
    
    for (num = 0; num <= n; num++) {
        printf("%d\n", fibonacciSeq(num));
    }
    return 0;
}

