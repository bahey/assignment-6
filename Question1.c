// By: A.R. Abdullah Al Bahey
// Course: Programming and Problem Solving
// Assignment on: Recursion
// Index No: 19020082




# include <stdio.h>

int final[100][100], row = 0, column = 0, count = 1;

void pattern(int n) {
    final[row][column] = count;
    if (count == 1) {
        row++;
        column = 0;
        count = row + 1;
    } else {
        count--;
        column++;
    }
    if (row < n) {
        pattern(n);
    }
}


int main() {
    int n, x, y;
    
    printf("How many lines you want to print ? ");
    scanf("%d", &n);
    pattern(n);
    
    printf("\n------------ Pattern ----------- \n\n");
    for (x = 0; x<n; x++) {
        for (y = 0; y<n; y++) {
            if (final[x][y] != 0) {
                printf("%d", final[x][y]);
            } else {
                printf("\n");
                break;
            }
        }
    }
    return 0;
}


